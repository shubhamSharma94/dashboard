import {
	CHANGE_THEME,
} from "../Constants.js";


const initialStateChangeTheme = {
    theme: "default",
  }
  
  
  export const changeTheme = (state = initialStateChangeTheme, action = {}) => {
    switch (action.type) {
      case CHANGE_THEME:
        // console.log("User value is changed")
        // let oldState = JSON.parse(JSON.stringify(state))

        return Object.assign({}, state, {
            theme: action.payload,
        })
      default:
        return state
    }
  }