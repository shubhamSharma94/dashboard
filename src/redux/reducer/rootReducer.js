import { combineReducers } from 'redux';
import { changeTheme } from "./changetheme";
import { changeActiveNav } from "./changeActiveNav"

const rootReducer = combineReducers({
    changeTheme,
    changeActiveNav
});

export default rootReducer;