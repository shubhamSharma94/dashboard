import {
	CHANGE_ACTIVE_NAV,
} from "../Constants.js";


const initialStateChangeActiveNav = {
    navItems: [
        {
            label: "Dashboard",
            url: "/",
            icon: "icons/computer.svg",
            className: "",
        },
        {
            label: "Products",
            url: "/",
            icon: "icons/product.svg",
            className: "",
        },
        {
            label: "Pricing",
            url: "/",
            icon: "icons/rupee.svg",
            className: "",
        },
        {
            label: "About",
            url: "/",
            icon: "icons/about.svg",
            className: "",
        },
        {
            label: "Help",
            url: "/",
            icon: "icons/support.svg",
            className: "",
        }
    ],
    activeNavItem: {},
    isOpen: false,
  }
  
  
  export const changeActiveNav = (state = initialStateChangeActiveNav, action = {}) => {
    switch (action.type) {
      case CHANGE_ACTIVE_NAV:
        // console.log("User value is changed")
        // let oldState = JSON.parse(JSON.stringify(state))

        return Object.assign({}, state, {
            activeNavItem: action.payload.activeNavItem,
            isOpen: action.payload.isOpen,
        })
      default:
        return state
    }
  }