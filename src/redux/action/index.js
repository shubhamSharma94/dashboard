import {
	CHANGE_THEME,
	CHANGE_ACTIVE_NAV
} from "../Constants.js";


export const changeTheme = (data) => {
  return {
    type: CHANGE_THEME,
    payload: data,

  }
}

export const changeActiveNav = (data) => {
  return {
    type: CHANGE_ACTIVE_NAV,
    payload: data,

  }
}
