import React, { PureComponent } from "react";
import "../../css/common.css";

class CustomList extends PureComponent {
    constructor (props) {
        super (props);
        this.state = {
            
        }
    }

    render () {
        let props = this.props;
        return (
            <ul className={`custom-list ${props.className ? props.className : ""}`}>
                {
                    props.list && props.list.map((elm, i) => {
                        return(
                            <li key={elm}>{elm}</li>
                        )
                    })
                }
            </ul>
        )
    }
}

export default CustomList;