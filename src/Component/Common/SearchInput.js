import React,  { PureComponent } from "react";
import "../../css/input.css";
class SearchInput extends PureComponent {
    constructor(props) {
        super (props);
        this.state = {

        }
    }

    render () {
        let props = this.props;
        return (
            <div className="inputbox search-input">
                <i className="fas fa-search"></i>
                <input 
                    type={props.type ? props.type : "text"}
                    // value={props.value ? props.value : ""}
                    onChange={props.handleInputChange}
                    onBlur={props.handleInputBlur}
                    placeholder={props.placeholder ? props.placeholder : "Search"}
                />
            </div>
        )
    }
}

export default SearchInput;