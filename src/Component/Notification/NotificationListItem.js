import React from "react";

function NotificationListItem (props) {
    return (
        <div className="notificaion-item">
            <div className="notification-content">
                <span className="notification-type-icon">
                    <i class="far fa-calendar"></i>
                </span>
                <p className="notification-text">This is a notification</p>
            </div>
            <span className="time-stamp">few seconds ago</span>
        </div>
    )
}

export default NotificationListItem;