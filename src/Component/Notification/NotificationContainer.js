import React, { PureComponent } from "react";
import NotificationListItem from "./NotificationListItem";
class NotificationContainer extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			isOpened: false,
		}
		this.toggleShowNotification = this.toggleShowNotification.bind(this);
	}

	toggleShowNotification() {
		this.setState({
			isOpened: !this.state.isOpened,
		}, () => {
            if (this.state.isOpened) {
                document.addEventListener("click", this.toggleShowNotification)
            } else {
                document.removeEventListener("click", this.toggleShowNotification)

            }
        })
	}

	render() {

		const NotificationItem = (data) => {
			return (
				<NotificationListItem data={data}/>
			)
		}
		return (
			<div className="notification-container">
				<span 
					className="notification-icon"
					onClick={this.toggleShowNotification}
				>
					<i class="far fa-bell"></i>
					<span className="notification-count">5</span>
				</span>
				<div className={`notification-card ${this.state.isOpened ? "open" : ""}`}>
					<div className="notification-title"></div>
					{NotificationItem()}
					{NotificationItem()}
					{NotificationItem()}
					{NotificationItem()}
					{NotificationItem()}
					{NotificationItem()}
					{NotificationItem()}
				</div>
			</div>
		)
	}
}

export default NotificationContainer;