import React, { PureComponent } from "react";
import Avatar from "./Avatar";
import CustomList from "../Common/CustomList";
export default class AvatarProfileList extends PureComponent {
    constructor (props) {
        super (props);
        this.state = {
            isOpened: false,
        }
        this.toggleProfileList = this.toggleProfileList.bind(this);
    }

    toggleProfileList () {
        this.setState ({
            isOpened: !this.state.isOpened,
        }, () => {
            if (this.state.isOpened) {
                document.addEventListener("click", this.toggleProfileList)
            } else {
                document.removeEventListener("click", this.toggleProfileList)

            }
        })
    }

    

    render() {
        return (
            <div className="profile-avatar">
            
                <Avatar 
                    handleClick={this.toggleProfileList}
                />
                <CustomList 
                    className={`profile-list ${this.state.isOpened ? "opened" : ""}`}
                    list={[
                        "one", "two", "three"
                    ]}
                />
            </div>
        )
    }
}