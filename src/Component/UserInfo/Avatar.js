import React from "react";
import "../../css/userInfo.css"

export default function Avatar(props) {
	return (
		<span 
			className="avatar"
			style={{
					backgroundImage: "Url(./images/avatar1.jpg)"
				}}
			onClick={props.handleClick} 
		>
		</span>
	)
}