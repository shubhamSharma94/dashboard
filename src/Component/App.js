import React, { PureComponent } from 'react';
import SidebarContainer from "./Sidebar/SidebarContainer";
import TopbarContainer from "./Topbar/TopbarContainer";
import GradientBox from "./BackgroundComponent/GradientBox";
import "../css/main.css";

class App extends PureComponent {
  constructor (props) {
    super (props);
    this.state = {

    }
  }

  render () {
    return (
      <div className="app">
        <SidebarContainer />
        <TopbarContainer />
        <div className="slate">
          <GradientBox />
        
        </div>
      </div>
    )
  }
}

export default App;
