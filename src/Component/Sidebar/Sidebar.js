import React , { PureComponent, Fragment } from "react";
import NavItem from "./NavItem";

export default class Sidebar extends PureComponent {
    constructor(props) {
        super (props);
        this.state = {

        }
    }

    render () {
        return (
            <Fragment>
                {
                    this.props.navItems && this.props.navItems.map((elm, i) => {
                        return (
                            <NavItem navData={elm} key={elm.label} />
                        )
                    })
                }
            </Fragment>
        )
    }
}