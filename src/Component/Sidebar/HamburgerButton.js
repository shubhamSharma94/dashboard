import React from "react";

function HamburgerButton(props) {
	return (
		<button className={`hamburger hamburger--squeeze ${props.isOpen ? "is-active" : ""}`} type="button" onClick={props.handleToggleSidebar}>
			<span className="hamburger-box">
				<span className="hamburger-inner"></span>
			</span>
		</button>
	)
}

export default HamburgerButton;