import React, { PureComponent } from "react";
import Sidebar from "./Sidebar";
import HamburgerButton from "./HamburgerButton";
import { connect } from "react-redux";
import { changeActiveNav } from "../../redux/action/index";
import "../../css/sidebar.css";


const mapStateToProps = state => {
	return {
		navItems: state.changeActiveNav.navItems,
		activeNavItem: state.changeActiveNav.activeNavItem,
		isOpen: state.changeActiveNav.isOpen,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		changeActiveNav: (obj) => dispatch(changeActiveNav(obj)),
	}
}


class SidebarComponent extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
		
		}
		this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
	}

	handleToggleSidebar () {
		let navObj = {};
		navObj["isOpen"] = !this.props.isOpen;
		navObj["activeNavItem"] = this.props.activeNavItem;
		this.props.changeActiveNav(navObj);
	}

	

	render() {

		const Hamburg = () => {
			return (
				<HamburgerButton
					handleToggleSidebar={this.handleToggleSidebar}
					isOpen={this.props.isOpen}
				/>
			)
		}

		return (
			<div className={`sidebar ${this.props.isOpen ? "open" : ""}`}>
				<Hamburg />
				<Sidebar
					navItems={this.props.navItems}
					activeNavItem={this.props.activeNavItem}
				/>
			</div>
		)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SidebarComponent);