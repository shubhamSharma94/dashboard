import React , { PureComponent } from "react";
import ReactSVG from 'react-svg';
import { connect } from "react-redux";
import { changeActiveNav } from "../../redux/action/index";

const mapStateToProps = state => {
	return {
		navItems: state.changeActiveNav.navItems,
		activeNavItem: state.changeActiveNav.activeNavItem,
		isOpen: state.changeActiveNav.isOpen,
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		changeActiveNav: (obj) => dispatch(changeActiveNav(obj)),
	}
}


class NavItem extends PureComponent {
    constructor(props) {
        super (props);
        this.state = {

        }
		this.handleNavItemClicked = this.handleNavItemClicked.bind(this);

    }

    handleNavItemClicked (label) {
		let navObj = {};
		navObj["isOpen"] = this.props.isOpen;

		let activeNavItem = {};
		let navItems = this.props.navItems;
		navItems && navItems.forEach(elm => {
			if (elm.label === label) {
				activeNavItem = elm;
			}
		});

		navObj["activeNavItem"] = activeNavItem;
		this.props.changeActiveNav(navObj);
	}

    render () {
        let navData = this.props.navData;
        let activeNavItemLabel = this.props.activeNavItem && this.props.activeNavItem.label;
        return (
            <div 
                className={`nav-item ${navData.className} ${activeNavItemLabel === navData.label ? "active" : ""}`}
                onClick={() => {
                    this.handleNavItemClicked(navData.label)
                }}
            >
                {
                    navData && navData.icon ? 
                    (
                        <ReactSVG 
                            src={navData.icon}  className="nav-icon"/>
                        
                    ) : null
                }
                <p>{navData.label}</p>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavItem);