import React, { PureComponent } from "react";
import AvatarProfileList from "../UserInfo/AvatarProfileList";
import SearchInput from "../Common/SearchInput";
import NotificationContainer from "../Notification/NotificationContainer";
// import MessageContainer from "../Message/MessageContainer";
import "../../css/topbar.css";
class TopbarContainer extends PureComponent {
    constructor (props) {
        super (props);
        this.state = {

        }
    }


    render () {
        return (
            <div className="topbar">
                <SearchInput />
                <div className="navbar-list">
                    <NotificationContainer />
                    <AvatarProfileList />
                    {/* <MessageContainer /> */}

                </div>
            </div>
        )
    }
}


export default TopbarContainer;